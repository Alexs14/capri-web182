/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umsa.web.listado_abm;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.umsa.domain.Clientes;
import org.umsa.domain.Proveedor;
import org.umsa.domain.Transaccion;
import org.umsa.domain.Devoluciones;

import org.umsa.domain.logic.MiFacade;

/**
 *
 * @author UMSA-JES
 */
public class Busqueda_Devolucion implements Controller {
    
    private MiFacade adqui;

    //String getCodUmsa;
    public void setAdqui(MiFacade adqui) {
        this.adqui = adqui;
    }
    
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {        
       //parametros={"nro":nro,"tipo":tipo,"unidad":unidad,"gestion":gestion};
                     
        String nro=request.getParameter("nro");
        String tipo=request.getParameter("tipo");
        String gestion=request.getParameter("gestion");
        
        
        System.out.println("Sergio Devolucion  : "+nro+" "+tipo+" "+gestion);
        
        Devoluciones a=new Devoluciones();
        //a.setCod_tramite(tipo);
        a.setGestion(gestion);
        a.setNro(nro);
        
        
        
        
        if(tipo.equals("1")){
                //realiza la ejecucion de la modificacion 1 
          response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        JSONObject Listado = new JSONObject();
        JSONArray datos = new JSONArray();
        JSONObject JSNDetalle;
        System.out.println("a: "+a.getNro()+"c: "+a.getGestion());
        Iterator i = this.adqui.listado_tramite_busqueda_uno(a).listIterator();
        System.out.println("a: "+nro+" b: "+tipo+" c: "+gestion);
        //System.out.println("El tamaño es --> "+this.adqui.busqueda(trans).size());
        Devoluciones aux;
        while (i.hasNext()) {
            JSNDetalle = new JSONObject();
            aux = (Devoluciones) i.next();
            /*out.println("<archivo>");
            out.println("<nombre_archivo>" + aux.getTerminos_ref() + "</nombre_archivo>");
            out.println("</archivo>");*/
            //  select temp1.unidad_solicitante,temp1.unidad_destino,temp1.detalle,temp1.fecha,temp1.nro,temp1.cod_transaccion,temp2.estado
  
            JSNDetalle.put("unidad_solicitante", aux.getUnidad_solicitante());
            JSNDetalle.put("unidad_destino", aux.getUnidad_destino());
            JSNDetalle.put("detalle", aux.getDetalle());
            JSNDetalle.put("fecha", obtencion_la_fecha(aux.getFecha()));
            JSNDetalle.put("nro", aux.getNro());
            System.out.println("Salida de la Fecha "+aux.getFecha());
             System.out.println("-----SALIDA DE FECHA JUNMIOR ----     "+obtencion_la_fecha(aux.getFecha()));;
    
            JSNDetalle.put("cod_transaccion", aux.getCod_transaccion());
            if(aux.getNro_orden_compra()!=null){
            JSNDetalle.put("nro_orden_compra", aux.getNro_orden_compra());
            
            }
            else{
            JSNDetalle.put("nro_orden_compra", "No Tiene");
            
            }
            JSNDetalle.put("estado", aux.getEstado());     
            JSNDetalle.put("cod_trans_nro", aux.getCod_trans_nro());
           
            System.out.println("========== Para Analizar ======" + JSNDetalle);
            datos.add(JSNDetalle);
        }
        //out.println("</listado>");
        Listado.put("conjunto", datos);
        System.out.println("===================== Yeah ======================");
        System.out.println(Listado.toJSONString());
        out.print(Listado);      
        }
        else{
                //realiza la ejecucion de la modificacion 2
             response.setContentType("application/json; charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        JSONObject Listado = new JSONObject();
        JSONArray datos = new JSONArray();
        JSONObject JSNDetalle;
        Iterator i = this.adqui.listado_tramite_busqueda_dos(a).listIterator();
        //System.out.println("El tamaño es --> "+this.adqui.busqueda(trans).size());
        Devoluciones aux;
        while (i.hasNext()) {
            JSNDetalle = new JSONObject();
            aux = (Devoluciones) i.next();
            /*out.println("<archivo>");
            out.println("<nombre_archivo>" + aux.getTerminos_ref() + "</nombre_archivo>");
            out.println("</archivo>");*/
            //  select temp1.unidad_solicitante,temp1.unidad_destino,temp1.detalle,temp1.fecha,temp1.nro,temp1.cod_transaccion,temp2.estado
  
            JSNDetalle.put("unidad_solicitante", aux.getUnidad_solicitante());
            JSNDetalle.put("unidad_destino", aux.getUnidad_destino());
            JSNDetalle.put("detalle", aux.getDetalle());
            JSNDetalle.put("fecha", obtencion_la_fecha(aux.getFecha()));
            JSNDetalle.put("nro", aux.getNro());
            JSNDetalle.put("cod_transaccion", aux.getCod_transaccion());
            JSNDetalle.put("estado", aux.getEstado());     
            JSNDetalle.put("nro_orden_compra", aux.getNro_orden_compra());
            JSNDetalle.put("cod_trans_nro", aux.getCod_trans_nro());
            
            
            System.out.println("-----SALIDA DE FECHA JUNMIOR ----     "+obtencion_la_fecha(aux.getFecha()));;
            System.out.println("========== Para Analizar ======" + JSNDetalle);
            datos.add(JSNDetalle);
        }
        //out.println("</listado>");
        Listado.put("conjunto", datos);
        System.out.println("===================== Yeah ======================");
        System.out.println(Listado.toJSONString());
        out.print(Listado);   
        }
        
        
        
        
        
        return null;
    }
    
    public String  obtencion_la_fecha(String fecha){
        StringTokenizer a=new StringTokenizer(fecha,"-");
        String anual=a.nextToken();
        String mes=a.nextToken();
        String dia=a.nextToken();
        StringTokenizer wer=new StringTokenizer(dia," ");
        String dia1=wer.nextToken();
        System.out.println(anual+" "+mes+" "+dia1);
        return dia1+"-"+mes+"-"+anual ;
    } 
}
