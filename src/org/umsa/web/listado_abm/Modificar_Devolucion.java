/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umsa.web.listado_abm;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static oracle.net.aso.C07.i;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.umsa.domain.logic.MiFacade;
import org.umsa.domain.Devoluciones;
/**
 *
 * @author UMSA-JES
 */
public class Modificar_Devolucion implements Controller {
    
    private MiFacade adqui;

    //String getCodUmsa;
    public void setAdqui(MiFacade adqui) {
        this.adqui = adqui;
    }
    
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {        
        
        String codigo_transaccion=request.getParameter("cod_transaccion");
        String cod_trans_nro=request.getParameter("cod_trans_nro");
        String estado=request.getParameter("estado");
        
        System.out.println("ENVIO DE MODIFICAION"+cod_trans_nro+" "+codigo_transaccion+" "+estado);
      
            Devoluciones uno=new Devoluciones();
            uno.setCod_trans_nro(cod_trans_nro);
           
        if(estado.equals("PPTO") ||estado.equals("ADQ") ){
            int valor=this.adqui.busqueda_transaccion_detalle(uno).size();
            for (int j = 0; j < valor; j++) {
                    Devoluciones aux=new Devoluciones();  
                    
                    aux = (Devoluciones) this.adqui.busqueda_transaccion_detalle(uno).get(j);
                    aux.setEstado("B");
                    
                    System.out.println("SALIDA finalñ de todo prioncipal "+aux.getEstado()+" "+aux.getCod_trans_detalle());
                    this.adqui.modificar_devolucion(aux);
            }
            
        }
        if(estado.equals("JUR") || estado.equals("C") || estado.equals("ALM") || estado.equals("ALM1") ){
           int valor1=this.adqui.busqueda_transaccion_detalle(uno).size();
            for (int j = 0; j < valor1; j++) {
                Devoluciones aux=new Devoluciones();                 
                    aux = (Devoluciones) this.adqui.busqueda_transaccion_detalle(uno).get(j);
                    aux.setEstado("ADQ");
                    System.out.println("SALIDA finalñ de todo prioncipal "+aux.getEstado()+" "+aux.getCod_trans_detalle());
                    
                    this.adqui.modificar_devolucion(aux);
            }
            
        }
        
        
        return new ModelAndView("inicio/Devoluciones");
    }
}
