/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umsa.web.listado_abm;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static oracle.net.aso.C07.i;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.umsa.domain.logic.MiFacade;
import org.umsa.domain.Devoluciones;
/**
 *
 * @author UMSA-JES
 */
public class Devolucion_Unidad_Solicitante implements Controller {
    
    private MiFacade adqui;

    //String getCodUmsa;
    public void setAdqui(MiFacade adqui) {
        this.adqui = adqui;
    }
    
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {        
        
        String codigo_transaccion=request.getParameter("cod_transaccion");
        Devoluciones uno=new Devoluciones();
        uno.setCod_transaccion(codigo_transaccion);
        uno.setEstado("B");
        
         System.out.println("DATA IMPORANTE PARA LA MODIFICACAON sergio ojo : "+codigo_transaccion);  
            this.adqui.devolucion_unidad_solicitante(uno);
     
        
        return new ModelAndView("inicio/Devoluciones");
    }
}
