/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umsa.domain;

import java.io.Serializable;

/**
 *
 * @author alex
 */
public class Devoluciones implements Serializable{
     // select temp1.unidad_solicitante,temp1.unidad_destino,temp1.detalle,temp1.fecha,temp1.nro,temp1.cod_transaccion,temp2.estado
  
    private String cod_tramite;
    private String tipo_reporte;
    private String unidad_solicitante;
    private String unidad_destino;
    private String detalle;
    private String fecha;
    private String nro;
    private String cod_transaccion;
    private String gestion;
    private String estado;
    private String cod_trans_nro;
    private String cod_trans_detalle;
    private String nro_orden_compra;

    public String getNro_orden_compra() {
        return nro_orden_compra;
    }

    public void setNro_orden_compra(String nro_orden_compra) {
        this.nro_orden_compra = nro_orden_compra;
    }

    public String getCod_trans_detalle() {
        return cod_trans_detalle;
    }

    public void setCod_trans_detalle(String cod_trans_detalle) {
        this.cod_trans_detalle = cod_trans_detalle;
    }

    public String getCod_trans_nro() {
        return cod_trans_nro;
    }

    public void setCod_trans_nro(String cod_trans_nro) {
        this.cod_trans_nro = cod_trans_nro;
    }
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getCod_tramite() {
        return cod_tramite;
    }

    public void setCod_tramite(String cod_tramite) {
        this.cod_tramite = cod_tramite;
    }

    public String getTipo_reporte() {
        return tipo_reporte;
    }

    public void setTipo_reporte(String tipo_reporte) {
        this.tipo_reporte = tipo_reporte;
    }

    public String getUnidad_solicitante() {
        return unidad_solicitante;
    }

    public void setUnidad_solicitante(String unidad_solicitante) {
        this.unidad_solicitante = unidad_solicitante;
    }

    public String getUnidad_destino() {
        return unidad_destino;
    }

    public void setUnidad_destino(String unidad_destino) {
        this.unidad_destino = unidad_destino;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNro() {
        return nro;
    }

    public void setNro(String nro) {
        this.nro = nro;
    }

    public String getCod_transaccion() {
        return cod_transaccion;
    }

    public void setCod_transaccion(String cod_transaccion) {
        this.cod_transaccion = cod_transaccion;
    }

    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }
    
    
    
    
    
    
}
