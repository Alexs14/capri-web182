/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.umsa.dao;

/**
 *
 * @author alex
 */

import java.util.List;
import org.springframework.dao.DataAccessException;
import org.umsa.domain.Devoluciones;

/**
 *
 * @author alex
 */
public interface DevolucionesDao {
    
    List listado_tramite() throws DataAccessException;
    List listado_tramite_busqueda_uno(Devoluciones d )throws DataAccessException;
    List listado_tramite_busqueda_dos(Devoluciones d )throws DataAccessException;
    
    void modificar_devolucion(Devoluciones d) throws DataAccessException;
    List busqueda_transaccion_detalle(Devoluciones d ) throws DataAccessException;   
    void devolucion_unidad_solicitante(Devoluciones d) throws DataAccessException;
    
    
//List getBuscaAdjudicados(adjudicados item) throws DataAccessException;
    /*public List BuscarAdjudicados(adjudicados i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    
}

