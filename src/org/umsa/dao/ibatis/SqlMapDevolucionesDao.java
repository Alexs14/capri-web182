/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.umsa.dao.ibatis;

import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.umsa.dao.DevolucionesDao;
import org.umsa.domain.Devoluciones;
/**
 *
 * @author alex
 */
public class SqlMapDevolucionesDao extends SqlMapClientDaoSupport implements DevolucionesDao{
    public List listado_tramite() throws DataAccessException {
        return getSqlMapClientTemplate().queryForList("listado_tramite", null);
    }
    public List listado_tramite_busqueda_uno(Devoluciones d ) throws DataAccessException {
        System.out.println("aqui entra 159");
        return getSqlMapClientTemplate().queryForList("busqueda_tramite_1", d);
    }
    public List listado_tramite_busqueda_dos(Devoluciones d ) throws DataAccessException {
        return getSqlMapClientTemplate().queryForList("busqueda_tramite_2", d);
    }
    
     public void modificar_devolucion(Devoluciones d ) throws DataAccessException {
       getSqlMapClientTemplate().update("modificar_devolucion", d);
    }
     
     //busqueda_transaccion_detalle
    public List busqueda_transaccion_detalle(Devoluciones d ) throws DataAccessException {
        return getSqlMapClientTemplate().queryForList("busqueda_transaccion_detalle", d);
    }
   public void devolucion_unidad_solicitante(Devoluciones d) throws DataAccessException {
          getSqlMapClientTemplate().update("devolucion_unidad_solicitante", d); 
    }
    
     
     
}
