<%-- 
    Document   : M
    Created on : 06-abr-2016, 17:51:27
    Author     : alex
--%>

<%@ include file="../Superior.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
       
        <script>
            $(document).ready(function(){
            //variable global           
                parametros={"nro":0,"tipo":0,"gestion":0};
                $("#tabla").hide();
                $("#buscar").click(function(){
                    
                    $("#cuerpo").empty();
                    
                    nro=$("#nro").val();
                    tipo=$("#tipo_solicitud").val();
                   
                    gestion=$("#gestion").val();
                    parametros={"nro":nro,"tipo":tipo,"gestion":gestion};
                    buscarAjax(parametros);
                });
                
                function buscarAjax(parametros){
                        $.ajax({
                            data:parametros,
                            url:"/capri-web/busqueda_devolucion.umsa",
                            type: 'POST',
                            dataType:"json",
                            success:function(json){
                                adicionarTabla(json);
                            }
                        });
                }
                
                function adicionarTabla(json){
                   
                   console.log(json);
                   if(json.conjunto.length>0){
                        $("#devolver_todo").show();
                        var tabla=$("#tabla");
                        $.each(json.conjunto, function(key,value) {
                        tabla.append(
                                 "<tr><td>"+value.unidad_solicitante+
                                 "</td><td>"+value.unidad_destino+
                                 "</td><td>"+value.detalle+
                                 "</td><td>"+value.fecha+
                                 "</td><td>"+value.nro+
                                 "</td><td>"+value.cod_transaccion+
                                 "</td><td>"+value.nro_orden_compra+
                                 "</td><td>"+value.estado+
                                 "</td><td>"+devolverBoton(value.cod_transaccion,value.cod_trans_nro,value.estado)+
                                 "</td><td>"+devolverUnidadSolcitante(value.cod_transaccion)+
                                 "</td></tr>");
                        });

                        $("#tabla").show();
                        $("input[name='devolver_todo_solicitud']").click(function(){
                            //estiloDevolucionNormal($(this));
                            cod_transaccion=$(this).attr("id");
                            
                            console.log("ENVIANDO A UNIDAD SOLICITANTE  : "+cod_transaccion);
                            
                           // parametro_actualizacion=parametros;
                            devolucionUnidadSolicitanteAjax(cod_transaccion);
                            
                        });
                        $("input[name='modificacion_solicitud']").click(function(){
                            cod_transaccion=$(this).attr("codTransaccion"); 
                            codTransNro=$(this).attr("codTransNro"); 
                            estado=$(this).attr("estado"); 
                            console.log("ENVIANDO A DEVOLUCIONDES EN DOL CODIGO Y TODO \n\
                    "+cod_transaccion+" "+codTransNro+" "+estado);
                          // parametro_actualizacion=parametros;
                            if(codTransNro=="null"){
                                console.log("es nulo");
                                 devolucionUnidadSolicitanteAjax(cod_transaccion);
                            }
                            else{
                                console.log("no es nulo");
                                console.log("ES MUY DIFERENETE "+estado );
                               devolucionGeneralAjax(cod_transaccion,codTransNro,estado);                   
                            }
                           
                            
                        });
                        
                    }
                    else{
                        $("#No_Existe_Elementos").dialog("open");   
                        $("#tabla").hide();
                    }
                    
                    
                    
                }
                
                function devolverBoton(cod_transaccion,cod_trans_nro,estado){
                    boton="<input type='submit'  codTransaccion='"+cod_transaccion+"' codTransNro='"+cod_trans_nro+"' estado='"+estado+"' name='modificacion_solicitud'  value='DEVOLVER'  />"  ;          
                    return  boton;
                    //return "<a href=/capri-web/modificar_transaccion_devolucion.umsa?cod_transaccion="+cod_transaccion+"&cod_trans_nro="+cod_trans_nro+"&estado="+estado+"> DEVOLVER</a>";
                }
                function devolverUnidadSolcitante(cod_transaccion){
                    //href=/capri -web/devolver_unidad_solicitante.umsa?cod_transaccion="+cod_transaccion
                    boton="<input type='submit'  id='"+cod_transaccion+"' name='devolver_todo_solicitud'  value='DEVOLVER UNIDAD SOLICITANTE'  />"  ;          
                    return  boton;
                }
                function devolucionUnidadSolicitanteAjax(cod_transaccion){
                    $.ajax({
                            data:{"cod_transaccion":cod_transaccion},
                            url:"/capri-web/devolver_unidad_solicitante.umsa",
                            type: 'POST',
                            success:function(){
                                actualizarPagina(parametros);
                            }
                     });
                }
                function devolucionGeneralAjax(cod_transaccion,codTransNro,estado){
                   console.log("ENVIANDO AJAX "+estado);
                    $.ajax({
                            data:{"cod_transaccion":cod_transaccion,"cod_trans_nro":codTransNro,"estado":estado},
                            url:"/capri-web/modificar_transaccion_devolucion.umsa",
                            type: 'POST',
                            success:function(){
                                actualizarPagina(parametros);
                            }
                     });
                }
                function actualizarPagina(parametro_actualizacion){
                    console.log("Nro: "+parametro_actualizacion.nro);
                    console.log("gestion : "+parametro_actualizacion.gestion);
                    console.log("tipo : "+parametro_actualizacion.tipo);
                    $("#cuerpo").empty();
                    buscarAjax(parametro_actualizacion);
                } 
               
                $("#devolver_todo").hide();
                $("#devolver_todo").click(function(){
                    //$("#devolver_todo").attr('href');              
                    console.log("ENVIAR SOLCITUD");  
                    
                    
                });
                
                $("#No_Existe_Elementos").dialog({
                   autoOpen:false,
                   width:400,
                   height:200,
                   position: "center",
                   resizable: false,
                   draggable: true,
                   hide:'puff',
                   show:'clip',
		   modal: true,
                   buttons : {
                            ACEPTAR : function ()
                            {
                                $("#No_Existe_Elementos").dialog("close");
                            },
                            
                     }
                   
                });
                
               
                
               
            });
        </script>
    </head>
    <body>
        <center> <h1>DEVOLUCIONES</h1></center>
            
             <div class="caja_busqueda">
                   <div class="form_busqueda">
                        <label> Nro. de Solicitud  : </label>
                        <input type="text" id="nro"/>
                        <label> Tipo de Solicitud  : </label>
                        <select id="tipo_solicitud">
                                <option value="1">SOLICITUD DE COMPRAS</option>                            
                                <option value="2">ORDEN DE COMPRA Y/U ORDEN DE SERVICIO</option>                            
                                <option value="3">INGRESO DE MATERIAL</option>                            
                                <option value="4">PEDIDO DE MATERIALES</option>                            
                                <option value="5">NOTA DE CONFORMIDAD</option>                            
                            
                        </select>
                        <label> Gestion : </label>
                        <input type="text" id="gestion"/>
                        <input type="submit" id="buscar" value="BUSCAR" class="btn_form"/>
                   </div>
            </div>
             <table id="tabla">
             <thead id="cabecera">
                 <th>Unidad Solcitante</th>
                 <th>Unidad Destino</th>
                 <th>Detalle</th>
                 <th>Fecha</th>
                 <th>Nro. de Solicitud</th>
                 <th>Codigo de Transaccion</th>
                 <th>Nro. Orden Compra</th>
                 <th>Estado</th>
                 <th>Operacion 1</th>
                 <th>Operacion 2</th>
                 
             </thead>
             <tbody id="cuerpo">
                     
                 </tbody>   
             </table>
                 
        
    </body>
    
    <div id="No_Existe_Elementos">
                Lamentablemente no existe Registros de esa Solicitud.
    </div>
    
   
    
    
    
</html>
